SimpleXY = {}
SimpleXY.Version = 0.4
SimpleXY.ForceUpgrade = 0.3
local windowName = "SimpleXY_Minimap"

function SimpleXY.Init()
	-- create ListedPrices if no saved settings
	if not SimpleXY_SavedData then 	
		SimpleXY.ResetToDefault()	
	end	
	
	-- determine if default settings should be loaded
	if SimpleXY_SavedData["version"] < SimpleXY.ForceUpgrade then
		-- do new version stuff
		SimpleXY.ResetToDefault()
		TextLogAddEntry( "Chat", filter, towstring( "SimpleXY: New Version Detected") );	
	end

	-- register slash commands
	if LibSlash then
		LibSlash.RegisterSlashCmd( "simplexy", function( args )SimpleXY.SlashCommand( args ) end )
		TextLogAddEntry( "Chat", filter, towstring( "Type /simplexy for options.") );	
	else
		TextLogAddEntry( "Chat", filter, towstring( "SimpleXY: LibSlash not installed. Slash commands disabled.") );		
	end		
	
	-- Hook Zone Map coords
	SimpleXY.BackupUpdateCoordinates = EA_Window_WorldMap.UpdateCoordinates
	EA_Window_WorldMap.UpdateCoordinates = SimpleXY.UpdateCoordinates
	SimpleXY.SetZoneMap()
	
	-- create minimap x,y label
	CreateWindow(windowName, true)
	LabelSetText(windowName.."Text", L"")	
	SimpleXY.SetMinimap(SimpleXY_SavedData["minimap_position"])
	
	-- update minimap on position update
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "SimpleXY.UpdateMiniMap");
	
	
end	

function SimpleXY.Shutdown()
	EA_Window_WorldMap.UpdateCoordinates = SimpleXY.BackupUpdateCoordinates
	UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "SimpleXY.UpdateMiniMap");
end


function SimpleXY.ResetToDefault()
	SimpleXY_SavedData = { }
	SimpleXY_SavedData["version"] = SimpleXY.Version
	SimpleXY_SavedData["minimap_precision"] = false
	SimpleXY_SavedData["zonemap_precision"] = true	
	SimpleXY_SavedData["minimap_position"] = "above"		
end

-- Slash command handler, requires LibSlash to be installed
function SimpleXY.SlashCommand(args)
	local opt, val = args:match( "([a-z0-9]+)[ ]?(.*)" )
	if (opt == "minimap" or opt == "zonemap") and (val == "precision" or val == "noprecision") then
		if val == "noprecision" then
			SimpleXY_SavedData[opt.."_precision"] = false
		elseif val == "precision" then
			SimpleXY_SavedData[opt.."_precision"] = true
		end
		SimpleXY.SetMinimap(SimpleXY_SavedData["minimap_position"])
		SimpleXY.UpdateMiniMap()
		TextLogAddEntry( "Chat", filter, towstring("SimpleXY: "..opt.." is "..val) );
	elseif opt == "minimap" and (val == "off" or val == "default" or val == "left" or val == "below") then
		SimpleXY.SetMinimap(val)
		TextLogAddEntry( "Chat", filter, towstring("SimpleXY: "..opt.." is "..val) );
	else
		TextLogAddEntry( "Chat", filter, towstring( "Usage:") );	
		TextLogAddEntry( "Chat", filter, towstring( "/simplexy [minimap] [off|default|left|below]") );					
		TextLogAddEntry( "Chat", filter, towstring( "/simplexy [minimap|zonemap] [precision|noprecision]") );
	end
end

-- converts to more digestible format
--where precision is a boolean used to determine format
function SimpleXY.Convert(num,precision)
	num = num / 1000
	if precision then
		return towstring(tostring(string.format("%.2f",num)))
	else
		return math.ceil(num - .5)
	end
end

function SimpleXY.UpdateCoordinates()
    if (EA_Window_WorldMap.currentLevel == GameDefs.MapLevel.ZONE_MAP) then
        local mapPositionX, mapPositionY = WindowGetScreenPosition("EA_Window_WorldMapZoneViewMapDisplay")
        local x, y = MapGetCoordinatesForPoint("EA_Window_WorldMapZoneViewMapDisplay",
                                               SystemData.MousePosition.x - mapPositionX,
                                               SystemData.MousePosition.y - mapPositionY)
                                               
	local playerX = SimpleXY.Convert(LibSurveyor.PlayerLocation.zoneX,SimpleXY_SavedData["zonemap_precision"])
	local playerY = SimpleXY.Convert(LibSurveyor.PlayerLocation.zoneY,SimpleXY_SavedData["zonemap_precision"])	
        	
        if (x == nil) then
            x = L"-"
        else
		x = SimpleXY.Convert(x,SimpleXY_SavedData["zonemap_precision"])
	end
	
        if (y == nil) then
            y = L"-"
	else
		y = SimpleXY.Convert(y,SimpleXY_SavedData["zonemap_precision"])
        end
  
	LabelSetText("EA_Window_WorldMapZoneViewCoordinates", L"Player: "..playerX..L", "..playerY..L"\nCursor: "..x..L", "..y) 
    end
end

function SimpleXY.SetZoneMap()
	-- save position
	local anchors = {}
	local numAnchors = WindowGetAnchorCount( "EA_Window_WorldMapZoneViewCoordinates" )
	for index = 1, numAnchors
	do
		local anchorPoint, relativeAnchorPoint, relativeToParent, anchorX, anchorY = WindowGetAnchor( "EA_Window_WorldMapZoneViewCoordinates", index )    
		anchors[index] = {}
		anchors[index].point = anchorPoint
		anchors[index].relativePoint = relativeAnchorPoint
		anchors[index].relativeTo = relativeToParent
		anchors[index].xoffs = anchorX
		anchors[index].yoffs = anchorY
	end

	-- move it into the new position
	WindowClearAnchors("EA_Window_WorldMapZoneViewCoordinates")
	for index = 1, numAnchors
	do
		local anchorY = anchors[index].yoffs
		if index == 1 then
			anchorY = anchorY - 14
		end
		WindowAddAnchor( "EA_Window_WorldMapZoneViewCoordinates", anchors[index].point, anchors[index].relativeTo, anchors[index].relativePoint, anchors[index].xoffs, anchorY )
	end	
end

function SimpleXY.UpdateMiniMap()
	local playerX = SimpleXY.Convert(LibSurveyor.PlayerLocation.zoneX,SimpleXY_SavedData["minimap_precision"])
	local playerY = SimpleXY.Convert(LibSurveyor.PlayerLocation.zoneY,SimpleXY_SavedData["minimap_precision"])	
	LabelSetText(windowName.."Text", L""..playerX..L", "..playerY)
end

function SimpleXY.SetMinimap(val)
	local xAbs = 13
	local yAbs = 0 	
	if SimpleXY_SavedData["percision"] then
		xAbs = 8
	end

	-- save our choice
	SimpleXY_SavedData["minimap_position"] = val
	
	-- hide it if off
	if val == "off" then
		WindowSetShowing( windowName, false )
		return
	end
	
	-- otherwise make sure it is displayed
	if not WindowGetShowing( windowName ) then
		WindowSetShowing( windowName, true )
	end
	
	-- anchor to new position
	WindowClearAnchors(windowName.."Text")
	if val == "left" then
		xAbs = xAbs + 50
		WindowAddAnchor(windowName.."Text", "bottomleft", "EA_Window_OverheadMap", "bottom", xAbs, yAbs)
	elseif val == "below" then
		yAbs = 10
		WindowAddAnchor(windowName.."Text", "bottom", "EA_Window_OverheadMap", "bottom", xAbs, yAbs)
	else
		WindowAddAnchor(windowName.."Text", "bottom", "EA_Window_OverheadMap", "bottom", xAbs, yAbs)
	end
end