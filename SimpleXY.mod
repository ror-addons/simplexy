<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SimpleXY" version="1.0" date="10/09/2008" >
		<Author name="sid67" email="none@private.com" />
		<Description text="Modifies how coordinates are displayed." />
		<Dependencies>
			<Dependency name="LibSurveyor" />
			<Dependency name="LibSlash" />
		</Dependencies>
		<Files>
		  <File name="SimpleXY.xml" />		  
		</Files>
		<SavedVariables>
			<SavedVariable name="SimpleXY_SavedData" />
		</SavedVariables>			
		<OnInitialize>
			<CallFunction name="SimpleXY.Init" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="SimpleXY.Shutdown" />
		</OnShutdown>		
	</UiMod>
</ModuleFile>